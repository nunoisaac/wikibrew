#French Press


French Press, also known as a cafetière, cafetière à piston, caffettiera a stantuffo, press pot, coffee press, or coffee plunger, is a coffee brewing device, although it can also be used for other tasks. In 1923 Ugo Paolini, an Italian, lodged patent documents relating to a tomato juice separator and he developed the idea of making a coffee pot with a press action and a filter. He assigned his 1928 patent to Italian designer Attilio Calimani and Giulio Moneta[1] who filed it in 1929.[2]

![french press](https://www.illy.com/content/dam/channels/website/consumer/global/coffee/preparation/french-press/2019_french-press_website_ENG_720x360.png)